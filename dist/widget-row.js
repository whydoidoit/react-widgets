"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = layoutRow;

var _reactstrap = require("reactstrap");

var _react = _interopRequireDefault(require("react"));

var _utilities = require("./utilities");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function layoutRow(_ref) {
  var _ref$left = _ref.left,
      left = _ref$left === void 0 ? [] : _ref$left,
      _ref$colType = _ref.colType,
      colType = _ref$colType === void 0 ? "md" : _ref$colType,
      style = _ref.style,
      centreWidth = _ref.centreWidth,
      _ref$leftWidth = _ref.leftWidth,
      leftWidth = _ref$leftWidth === void 0 ? 3 : _ref$leftWidth,
      _ref$rightWidth = _ref.rightWidth,
      rightWidth = _ref$rightWidth === void 0 ? 3 : _ref$rightWidth,
      _ref$leftClass = _ref.leftClass,
      leftClass = _ref$leftClass === void 0 ? "" : _ref$leftClass,
      _ref$rightClass = _ref.rightClass,
      rightClass = _ref$rightClass === void 0 ? "" : _ref$rightClass,
      _ref$centreClass = _ref.centreClass,
      centreClass = _ref$centreClass === void 0 ? "" : _ref$centreClass,
      _ref$centre = _ref.centre,
      centre = _ref$centre === void 0 ? [] : _ref$centre,
      _ref$right = _ref.right,
      right = _ref$right === void 0 ? [] : _ref$right,
      design = _ref.design,
      document = _ref.document,
      props = _ref.props,
      className = _ref.className;

  var leftInfo = _defineProperty({}, colType, leftWidth);

  var centreInfo = _defineProperty({}, colType, centreWidth || true);

  var rightInfo = _defineProperty({}, colType, rightWidth);

  return left.length === 0 && centre.length === 0 && right.length === 0 ? null : _react.default.createElement(_reactstrap.Row, {
    className: className
  }, !!left.length && _react.default.createElement(_reactstrap.Col, _extends({}, leftInfo, {
    style: {
      overflowY: "auto"
    }
  }), _react.default.createElement("div", {
    className: leftClass
  }, (0, _utilities.processRenderFunctions)(left).map(function (Item, key) {
    return _react.default.createElement(Item, {
      key: key
    });
  }))), _react.default.createElement(_reactstrap.Col, _extends({}, centreInfo, {
    style: Object.assign({
      overflowY: "auto"
    }, style)
  }), !!centre.length && _react.default.createElement("div", {
    className: centreClass
  }, (0, _utilities.processRenderFunctions)(centre).map(function (Item, key) {
    return _react.default.createElement(Item, {
      key: key
    });
  }))), !!right.length && _react.default.createElement(_reactstrap.Col, _extends({}, rightInfo, {
    style: {
      overflowY: "auto"
    }
  }), _react.default.createElement("div", {
    className: rightClass
  }, (0, _utilities.processRenderFunctions)(right).map(function (Item, key) {
    return _react.default.createElement(Item, {
      key: key
    });
  }))));
}