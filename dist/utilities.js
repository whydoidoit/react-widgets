"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.noop = noop;
exports.calculateBreakpoint = calculateBreakpoint;
exports.processRenderFunctions = processRenderFunctions;
exports.inPriorityOrder = inPriorityOrder;
exports.Wrapper = void 0;

function noop() {}

function inPriorityOrder(a, b) {
  return (a.priority || 1) - (b.priority || 1);
}

function processRenderFunctions(list, context) {
  list.sort(inPriorityOrder);

  if (context && context.debug) {}

  var renderFunctionList = list.filter(function (item) {
    return item.onBeforeRender;
  });
  renderFunctionList.forEach(function (item) {
    var result = item.onBeforeRender(list);
    list = Array.isArray(result) ? result : list;
  });
  return list;
}

function calculateBreakpoint(width) {
  if (width < 576) return "xs";
  if (width < 768) return "sm";
  if (width < 992) return "md";
  if (width < 1200) return "lg";
  return "xl";
}

var Wrapper = function Wrapper(props) {
  return props.children || null;
};

exports.Wrapper = Wrapper;