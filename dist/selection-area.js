"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SelectionArea = void 0;

var _reactJss = _interopRequireDefault(require("react-jss"));

var _whenSelected = require("./when-selected");

var _widgetStyles = require("./widget-styles");

var _classnames = _interopRequireDefault(require("classnames"));

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SelectionArea = (0, _reactJss.default)(_widgetStyles.styles)(function SelectionArea(_ref) {
  var classes = _ref.classes,
      _ref$visible = _ref.visible,
      visible = _ref$visible === void 0 ? true : _ref$visible;
  return !visible ? null : _react.default.createElement(_whenSelected.WhenSelected, null, _react.default.createElement("div", {
    className: (0, _classnames.default)(classes.widgetFrame, {
      active: true
    })
  }, "\xA0"));
});
exports.SelectionArea = SelectionArea;