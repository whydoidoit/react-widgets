module.exports = function override(config, env) {
    config.output.chunkFilename = "chunks/[name].chunk.js"
    return config
}
