This tutorial takes you through creating a simple editor for a survey, if you are looking to understand the principles behind the library then check out the {@tutorial Core Concepts} tutorial.

It's easy to add this component to a project created by [create-react-app](https://github.com/facebook/create-react-app).

### Create a Project

```shell
npx create-react-app my-app
cd my-app
```

### Install the widgets

```shell
npm install --save react-ioc-widgets
```

### Ensure the right version of React

Requires React 16.7 with hooks - which is currently `react@next`

```shell
npm install --save react@next
npm install --save react-dom@next
```

### Basic Example

In this example we are going to build all of the elements and import them directly, using the {@link globalWidgets} to handle events. See {@tutorial Hooks and Events} to see a discussion and example of how to dynamically load modules and use different {@link WidgetEvents} emitters.

### A Source Design

Here is a basic design for a survey with two types of question. We'll use this as our design.


**design.json**
```json
{
  "type": "survey",
  "title": "Personal Details",
  "questions": [
    {
      "id": "4b3WR_gPy",
      "name": "name",
      "question": "Full Name",
      "type": "text",
      "placeholder": "e.g. Mr Michael Talbot"
    },
    {
      "id": "XvzxOnLrg",
      "name": "email",
      "question": "Email Address",
      "type": "text",
      "placeholder": "someone@example.com"
    },
    {
      "id": "ZQd6x_vVQ",
      "name": "gender",
      "question": "Gender",
      "type": "list",
      "answers": [
        {
          "value": "m",
          "text": "Male"
        },
        {
          "value": "f",
          "text": "Female"
        }
      ]
    },
    {
      "id": "3RWcrVIQw",
      "name": "maritalStatus",
      "question": "Marital Status",
      "type": "list",
      "answers": [
        {
          "value": "s",
          "text": "Single"
        },
        {
          "value": "m",
          "text": "Married"
        },
        {
          "value": "d",
          "text": "Divorced"
        },
        {
          "value": "s",
          "text": "Separated"
        }
      ]
    }
  ]
}

```

### Creating The App Component

**App.js**

```jsx
import React from 'react'
import {Widgets, EditorFrame} from "react-ioc-widgets"
import design from "./design.json"

let document = {}

function Application() {
    //The design has the type "survey" so no need for type="survey"
    return <Widgets document={document} design={design}>
        <EditorFrame/>
    </Widgets>   
}

export default Application

```

This creates a Widgets to render our design, and adds an editor to it.  At the present time it's going
to be kinda empty!

### Start the server

```shell
npm start
```

### Display the survey

**survey.js**
```jsx
import React from 'react'
import {globalWidgets, Widget, useDesign, useDocument, useLayout} from "react-ioc-widgets"

function Survey() {
   const [design] = useDesign()
   const [document] = useDocument()
   return <>
       <h1>{design.title}</h1>
       {design.questions.map(question=>{
           return <Widget
              key={question.id} 
              design={question}
              type={`question.${question.type}`} 
              value={document[question.name]}
              setValue={(value)=>document[question.name] = value} 
              />
       })}
   </>
}

globalWidgets.configure("survey", function() {
    useLayout({content: [Survey]})
})

```

Here we create a component to represent the UI of our survey, it displays the title and then
creates a widget for each question. We are using `value` and `setValue` so we can use {@link useValue} in
our editors to update the value of each question.

We use {@link globalWidgets} and configure for the survey by adding our UI to the contents.

To get this to work we need to import this file

**App.js**

```jsx
import React from 'react'
import {Widgets, EditorFrame} from "react-ioc-widgets"
import design from "./design.json"
//Import the survey component
import "./survey"

...
```

At this point if we run the app then it will show the title, still nothing to display
questions.

### Display and update text questions

**text-question.js**

```jsx
import React from 'react'
import {globalWidgets, useDesign, useValue, useContent} from "react-ioc-widgets"

function TextQuestion() {
    const [design] = useDesign()
    const [value, setValue] = useValue()
    return <label>
        <p>{design.question}</p>
        <input value={value || ""} onChange={setValue}/>
    </label>
}

globalWidgets.render("question.text", function() {
    useContent(TextQuestion)
})

```

This provides a basic input editor, with a label set to the question.  We need to import it too.

**App.js**

```jsx
import React from 'react'
import {Widgets, EditorFrame} from "react-ioc-widgets"
import design from "./design.json"
import "./survey"
//Add the text question
import "./text-question"

...
```

Now the document is editable for the text questions!

### Change Notification

Lets make sure we know it's changed - we add an update handler to the {@link Widgets} component.

**App.js**

```jsx
import React from 'react'
import {Widgets, EditorFrame} from "react-ioc-widgets"
import design from "./design.json"
import "./survey"
import "./text-question"

let document = {}

//Log the document on each change
function changed() {
   console.log(document)
}

function Application() {
    //The design has the type "survey" so no need for type="survey"
    return <Widgets onUpdated={changed} document={document} design={design}>
        <EditorFrame/>
    </Widgets>   
}

export default Application
```

The onUpdated handler is called each time a change happens.

### Handling the list type

Now lets add a handler to display list questions

**list-question.js**
```jsx
import React from 'react'
import {globalWidgets, useDesign, useValue, useContent} from "react-ioc-widgets"

function ListQuestion() {
    const [{question, answers}] = useDesign()
    const [value, setValue] = useValue()
    return <label>
        <p>{question}</p>
        <select value={value || ""} onChange={setValue}>
           {answers.map(answer=><option key={answer.value} value={answer.value}>{answer.text}</option>)}
        </select>
    </label>
}

globalWidgets.render("question.list", function() {
    useContent(ListQuestion)
})
```

**App.js**

```jsx
import React from 'react'
import {Widgets, EditorFrame} from "react-ioc-widgets"
import design from "./design.json"
import "./survey"
import "./text-question"
//Add the list question
import "./list-question"

...
```

We can now edit all the questions!

### Adding an Edit Interface

Lets enable the editing experience! We can create an editor for the document title

**edit-title.js**
```jsx
import React from 'react'
import {globalWidgets, useDesign, useTabs} from "react-ioc-widgets"

function EditTitle() {
    const [{title}, update] = useDesign()
    return <label>
        <p>Document Title</p>
        <input value={title || ""} onChange={e=>update({title: e.target.value})}/>
    </label>
}

globalWidgets.editor("survey", function() {
    useTabs(EditTitle) //Add to general
})

```

Add an editor for question text, this needs to use the currently
focused question and only activate if it has a question property already.

**edit-question.js**
```jsx
import React from 'react'
import {globalWidgets, useDesign, useTabs} from "react-ioc-widgets"

function EditQuestion() {
    const [{question}, update] = useFocused()
    return <label>
        <p>Question</p>
        <input value={question || ""} onChange={e=>update({question: e.target.value})}/>
    </label>
}

globalWidgets.editor("survey", function() {
    const [question] = useFocused()
    question && question.question !== undefined && useTabs(EditQuestion) //Add to general
})
```

Import the components and add the editors

**App.js**
```jsx
import React from 'react'
import {Widgets, EditorFrame} from "react-ioc-widgets"
import design from "./design.json"
import "./survey"
import "./text-question"
import "./edit-question"
import "./edit-title"

let document = {}

//Log the document on each change
function changed() {
   console.log(document, design)
}

function Application() {
    //The design has the type "survey" so no need for type="survey"
    return <Widgets onUpdated={changed} document={document} design={design} editable={true}>
        <EditorFrame onUpdated={changed}/>
    </Widgets>   
}

export default Application
```
We add `editable={true}` to the {@link Widgets} component.

Now we can enable editor mode, modify the design and get updates.

### Adding New Items

In order to add new items, we really want a {@link Widget} to represent the actual list as well as the individual questions.

So we update the core renderer.

**survey.js**
```jsx
import React from 'react'
import {globalWidgets, Widget, useDesign, useDocument, useLayout} from "react-ioc-widgets"

function Survey() {
   const [design] = useDesign()
   const [document] = useDocument()
   return <>
       <h1>{design.title}</h1>
       //Add a frame for the questions
       <Widget type="questions" design={design.questions} document={document}>
           {design.questions.map(question=>{
               return <Widget
                  key={question.id} 
                  design={question}
                  type={`question.${question.type}`} 
                  value={document[question.name]}
                  setValue={(value)=>document[question.name] = value} 
                  />
           })}
       </Widget>
   </>
}

globalWidgets.configure("survey", function() {
    useLayout({content: [Survey]})
})

```

Now we can add an editor inline for the questions.  To invert the control we need to fire an event to find the different questions we can add.

**add-questions.js**
```jsx
import React from 'react'
import {globalWidgets, useDesign, useInline} from "react-ioc-widgets"

function AddQuestion() {
   const types = []
   const [questions, update] = useDesign()
   globalWidgets.emit("questiontypes", types)
   return <>
       {types.map(type=><button key={type.caption} onClick={()=>{
           questions.push(type.create())
           update()
       }}>{type.caption}</button>}
   </>
}

globalWidgets.render("questions", function() {
    useInline(AddQuestion)
})

```

We create a new event called `questiontypes` to populate the list of available questions.  We need to handle that:

**text-question.js**
```jsx
import React from 'react'
import {globalWidgets, useDesign, useValue, useContent} from "react-ioc-widgets"

globalWidgets.on("questiontypes", function(types) {
    types.push({caption: "Text", create: ()=>({type: "text", question: "New List Question"})})
})

...
```

### Next Steps

From this point on, we'd add a creation function for the list, an editor tab to edit the possible answers for a list etc.

