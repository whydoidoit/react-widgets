"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useAsync = useAsync;
exports.default = void 0;

var _react = require("react");

var _isFunction = _interopRequireDefault(require("lodash/isFunction"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var GUARD_VALUE = "_guard_";
/**
 * Returns null until some value or async function is resolved,
 * causes a React redraw when resolved.
 * @param {(Function|Promise|any)} promiseProducingFunction - either an async function, a promise or a value
 * @returns any
 * @example
 * function App() {
   let hasComponents = useAsync(async () => {
        try {
            await Promise.all(components.map(async item => {
                let module = await import(`./examples/${item}`)
                module.default(globalWidgets)
            }))
        } catch(e) {
            console.error(e.stack)
        }
        return true
    })
    return !hasComponents ? <div>Wait...</div> : <Widgets design={design} document={document} type="someType"/>
}

 */

function useAsync(promiseProducingFunction) {
  var _useState = (0, _react.useState)(GUARD_VALUE),
      _useState2 = _slicedToArray(_useState, 2),
      value = _useState2[0],
      setValue = _useState2[1];

  if (value === GUARD_VALUE) {
    var tempValue = promiseProducingFunction;
    promiseProducingFunction = !(0, _isFunction.default)(promiseProducingFunction) ?
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee() {
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              return _context.abrupt("return", tempValue);

            case 1:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    })) : promiseProducingFunction;
    setValue(null);
    value = null;
    Promise.resolve(promiseProducingFunction()).then(function (v) {
      setValue(v);
    });
  }

  return value;
}

var _default = useAsync;
exports.default = _default;