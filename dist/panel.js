"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Panel = Panel;

var _utilities = require("./utilities");

var _react = _interopRequireDefault(require("react"));

var _reactstrap = require("reactstrap");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function defaultChild(list, props) {
  return _react.default.createElement(_reactstrap.Col, props, list);
}

function Panel(_ref) {
  var _ref$visible = _ref.visible,
      visible = _ref$visible === void 0 ? true : _ref$visible,
      list = _ref.list,
      children = _ref.children,
      props = _objectWithoutProperties(_ref, ["visible", "list", "children"]);

  children = children || defaultChild;

  if (visible && list && list.length) {
    return children((0, _utilities.processRenderFunctions)(list).map(function (Item, index) {
      return _react.default.createElement(Item, {
        key: index
      });
    }), props);
  } else {
    return null;
  }
}