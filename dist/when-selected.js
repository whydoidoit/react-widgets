"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WhenSelected = WhenSelected;

var _react = _interopRequireWildcard(require("react"));

var _contexts = require("./contexts");

var _utilities = require("./utilities");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/**
 * WhenSelected provides a quick way to have content hide when
 * the user navigates away from the contents of a {@link Widget}
 * @kind component
 * @classdesc Use the WhenSelected component to only display content
 * when the {@link Widgets} component is focused on the parent {@link Widget}.
 * @example
 * return <WhenSelected>
 *     <span>I only appear when focused</span>
 * </WhenSelected>
 */
function WhenSelected(_ref) {
  var children = _ref.children;

  var _useContext = (0, _react.useContext)(_contexts.WidgetContext),
      _isSelected = _useContext._isSelected;

  return (0, _contexts.useFocus)(_isSelected)[0] ? _react.default.createElement(_utilities.Wrapper, null, children) : _react.default.createElement(_utilities.Wrapper, null);
}